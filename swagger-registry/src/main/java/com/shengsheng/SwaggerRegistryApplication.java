package com.shengsheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *
 * @author shengshenglalala
 * @date 2021/8/24 11:54
*/
@SpringBootApplication
@EnableEurekaServer
public class SwaggerRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(SwaggerRegistryApplication.class,args);
    }
}
