package com.shengsheng.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:20
 */

/**
 * @ApiModel 用于响应实体类上，用于说明实体作用
 *      description="描述实体的作用"
 *      value="给该类取一个替代名称"
 */
@ApiModel(value = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)

public class User implements Serializable {
    public User() {
    }

    public User(Long id, String name, Date birthday) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
    }

    /**
     * @ApiModelProperty 用在属性上，描述实体类的属性
     *      value="用户名"  描述参数的意义
     *      name="name"    参数的变量名
     *      required=true     参数是否必选
     *      example="0" 参数可以传递的值的举例
     *      allowableValues= range[1, 5], range(1, 5), range[1, 5),range[1, infinity] 允许传递的值的范围
     *      hidden=true 描述该字段在swagger文档中是否展示
     */
    @ApiModelProperty(value = "主键id" , example = "0")
    private Long id;

    @ApiModelProperty(value = "用户名" , example = "张三")
    private String name;

    @ApiModelProperty(value = "生日" , example = "2021-08-15 00:00:00")
    private Date birthday;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
