package com.shengsheng.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Optional;
import java.util.function.Function;


/**
 * swagger的配置
 * @author shengshenglalala
 * @date 2021/8/26 9:10
*/
@Configuration
/**
 * @EnableSwagger2 指定开启swagger文档
 */
@EnableSwagger2
public class SwaggerConfig {

    /**
     * 定义分隔符
     */
    private static final String SPLITER = ";";
    /**
     * 一个文档分组 有多个分组就可以指定多个Docket
     */
    @Bean
    public Docket allApi() {
        //分组名称可以随便定义，公司所有项目都用的这个分组名称所以我这里沿用
        String groupName = "RestfulApi";
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                //配置需要携带token才能访问
                //.securitySchemes(Lists.newArrayList(new ApiKey("token", "token", "header")))
                //范型覆盖 ResponseEntity<User> 将被替换成User
                .genericModelSubstitutes(ResponseEntity.class)
                //是否使用默认的响应消息
                .useDefaultResponseMessages(true)
                .forCodeGeneration(false)
                .select()
                //指定扫描规则
                //指定只扫描以下包名
                //.apis(basePackage("com.shengsheng.controller.UserController"+SPLITER+"com.shengsheng.controller.StudentController"))
                //这里可以根据url正则匹配 可以指定包名以及其他的规则
                .paths(Predicates.not(PathSelectors.regex("/actuator.*")))
                //.paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title("swagger-backend项目所有接口")
                        .description("swagger-backend项目所有接口")
                        .termsOfServiceUrl("")
                        .version("1.0.0")
                        .build());

    }

    public static Predicate<RequestHandler> basePackage(final String basePackage) {
        return input -> declaringClass(input).map(handlerPackage(basePackage)).orElse(true);
    }

    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage) {
        return input -> {
            // 循环判断匹配
            for (String strPackage : basePackage.split(SPLITER)) {
                boolean isMatch = input.getName().startsWith(strPackage);
                if (isMatch) {
                    return true;
                }
            }
            return false;
        };
    }

    private static Optional<Class<?>> declaringClass(RequestHandler input) {
        return Optional.ofNullable(input.declaringClass());
    }
}
