package com.shengsheng.service;

import com.shengsheng.pojo.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:24
 */
@Service
public class UserService {

    public List<User> findAllUser() {
        User user1 = new User(1L,"张三",new Date());
        User user2 = new User(2L,"李四",new Date());
        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        return userList;
    }

    public User findById(Long id) {
        return new User(id,"findById",new Date());
    }
}
