package com.shengsheng.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:20
 */
@ApiModel(value = "student",description = "学生实体类")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Student implements Serializable {
    public Student() {
    }

    public Student(Long id, String name, Date birthday) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
    }

    @ApiModelProperty(value = "主键id" , example = "0",name = "test")
    private Long id;

    @ApiModelProperty(value = "学生名" , example = "李四")
    private String name;

    @ApiModelProperty(value = "生日" , example = "2021-08-15 00:00:00")
    private Date birthday;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
